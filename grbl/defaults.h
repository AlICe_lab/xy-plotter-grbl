/*
defaults.h - defaults settings configuration file
Part of Grbl

Copyright (c) 2012-2016 Sungeun K. Jeon for Gnea Research LLC

Grbl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Grbl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Grbl.  If not, see <http://www.gnu.org/licenses/>.
*/

/* The defaults.h file serves as a central default settings selector for different machine
types, from DIY CNC mills to CNC conversions of off-the-shelf machines. The settings
files listed here are supplied by users, so your results may vary. However, this should
give you a good starting point as you get to know your machine and tweak the settings for
your nefarious needs.
NOTE: Ensure one and only one of these DEFAULTS_XXX values is defined in config.h */

#ifndef defaults_h

#ifdef DEFAULTS_GENERIC
// Grbl generic default settings. Should work across different machines.
#define DEFAULT_X_STEPS_PER_MM 250.0
#define DEFAULT_Y_STEPS_PER_MM 250.0
#define DEFAULT_Z_STEPS_PER_MM 250.0
#define DEFAULT_X_MAX_RATE 500.0 // mm/min
#define DEFAULT_Y_MAX_RATE 500.0 // mm/min
#define DEFAULT_Z_MAX_RATE 500.0 // mm/min
#define DEFAULT_X_ACCELERATION (10.0*60*60) // 10*60*60 mm/min^2 = 10 mm/sec^2
#define DEFAULT_Y_ACCELERATION (10.0*60*60) // 10*60*60 mm/min^2 = 10 mm/sec^2
#define DEFAULT_Z_ACCELERATION (10.0*60*60) // 10*60*60 mm/min^2 = 10 mm/sec^2
#define DEFAULT_X_MAX_TRAVEL 200.0 // mm NOTE: Must be a positive value.
#define DEFAULT_Y_MAX_TRAVEL 200.0 // mm NOTE: Must be a positive value.
#define DEFAULT_Z_MAX_TRAVEL 200.0 // mm NOTE: Must be a positive value.
#define DEFAULT_SPINDLE_RPM_MAX 1000.0 // rpm
#define DEFAULT_SPINDLE_RPM_MIN 0.0 // rpm
#define DEFAULT_STEP_PULSE_MICROSECONDS 10
#define DEFAULT_STEPPING_INVERT_MASK 0
#define DEFAULT_DIRECTION_INVERT_MASK 0
#define DEFAULT_STEPPER_IDLE_LOCK_TIME 25 // msec (0-254, 255 keeps steppers enabled)
#define DEFAULT_STATUS_REPORT_MASK 1 // MPos enabled
#define DEFAULT_JUNCTION_DEVIATION 0.01 // mm
#define DEFAULT_ARC_TOLERANCE 0.002 // mm
#define DEFAULT_REPORT_INCHES 0 // false
#define DEFAULT_INVERT_ST_ENABLE 0 // false
#define DEFAULT_INVERT_LIMIT_PINS 0 // false
#define DEFAULT_SOFT_LIMIT_ENABLE 0 // false
#define DEFAULT_HARD_LIMIT_ENABLE 0  // false
#define DEFAULT_INVERT_PROBE_PIN 0 // false
#define DEFAULT_LASER_MODE 0 // false
#define DEFAULT_HOMING_ENABLE 0  // false
#define DEFAULT_HOMING_DIR_MASK 0 // move positive dir
#define DEFAULT_HOMING_FEED_RATE 25.0 // mm/min
#define DEFAULT_HOMING_SEEK_RATE 500.0 // mm/min
#define DEFAULT_HOMING_DEBOUNCE_DELAY 250 // msec (0-65k)
#define DEFAULT_HOMING_PULLOFF 1.0 // mm
#endif

#ifdef DEFAULTS_ALICE_PLOTTER
// Description: AlICe's plotter.
#define DEFAULT_STEP_PULSE_MICROSECONDS 10 //$0
#define DEFAULT_STEPPER_IDLE_LOCK_TIME 25 // $1 msec (0-254, 255 keeps steppers enabled)
#define DEFAULT_STEPPING_INVERT_MASK 0 // $2
#define DEFAULT_DIRECTION_INVERT_MASK 5 // $3
#define DEFAULT_INVERT_ST_ENABLE 0 // $4 false
#define DEFAULT_INVERT_LIMIT_PINS 0 // $5 false
#define DEFAULT_INVERT_PROBE_PIN 0 // $6 false
#define DEFAULT_STATUS_REPORT_MASK 31 // $10 MPos enabled
#define DEFAULT_JUNCTION_DEVIATION 0.01 // $11 mm
#define DEFAULT_ARC_TOLERANCE 0.002 // $12 mm
#define DEFAULT_REPORT_INCHES 0 // $13 false
#define DEFAULT_SOFT_LIMIT_ENABLE 0 // $20 false
#define DEFAULT_HARD_LIMIT_ENABLE 1  // $21 false
// Homing
#define DEFAULT_HOMING_ENABLE 1  // $22 true
#define DEFAULT_HOMING_DIR_MASK 3 // $23 move negative dir
#define DEFAULT_HOMING_FEED_RATE 50.0 // $24 mm/min
#define DEFAULT_HOMING_SEEK_RATE 4000.0 // $25 mm/min
#define DEFAULT_HOMING_DEBOUNCE_DELAY 250 // $26 msec (0-65k)
#define DEFAULT_HOMING_PULLOFF 1.0 // $27 mm
// Head
#define DEFAULT_SPINDLE_RPM_MAX 1.0 // $30 rpm
#define DEFAULT_SPINDLE_RPM_MIN 0.0 // $31 rpm
#define DEFAULT_LASER_MODE 0 // $32 false
// Speed and Acceleration
#define DEFAULT_X_STEPS_PER_MM 80.0 //$100
#define DEFAULT_Y_STEPS_PER_MM 80.0 //$101
#define DEFAULT_Z_STEPS_PER_MM 80.0 //$102
#define DEFAULT_X_MAX_RATE 25000.0 // $110 mm/min
#define DEFAULT_Y_MAX_RATE 25000.0 // $111 mm/min
#define DEFAULT_Z_MAX_RATE 10000.0 // $112 mm/min       TODO
#define DEFAULT_X_ACCELERATION 600.0 // $120 10*60*60 mm/min^2 = 10 mm/sec^2
#define DEFAULT_Y_ACCELERATION 600.0 // $121 10*60*60 mm/min^2 = 10 mm/sec^2
#define DEFAULT_Z_ACCELERATION 600.0 // $122        TODO 10*60*60 mm/min^2 = 10 mm/sec^2
#define DEFAULT_X_MAX_TRAVEL 986.0 // $130 mm NOTE: Must be a positive value.
#define DEFAULT_Y_MAX_TRAVEL 981.0 // $131 mm NOTE: Must be a positive value.
#define DEFAULT_Z_MAX_TRAVEL 11.0 // $132

#endif

#endif
